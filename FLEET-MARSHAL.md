##### Flet Marshall

### 1) Jenkins job:

##  1. add repositories git and branch 

##  2. execute sh skript:


cp blog/docker/containers/db/.env.example blog/docker/containers/db/.env

cp blog/docker/containers/app/.env.example blog/docker/containers/app/.env

cp blog/docker/containers/db/.env.example blog/docker/containers/db/.env

cp blog/docker-compose-dev.yml.dist  blog/docker-compose-dev.yml

cd blog

composer install

npm install && npm install -G gulp && npm rebuild node-sass && npm run dev && node node_modules/gulp/bin/gulp.js
 
##  3. Send files to server and execute ssh command(run scripts)

/home/fleet/www/fleetmarshal/create.sh

create.sh: check folder and check if it's need


if ! [ -d /home/fleet/www/fleetmarshal/blog/storage ]; then

mkdir  /home/fleet/www/fleetmarshal/blog/storage

fi

if ! [ -d /home/fleet/www/fleetmarshal/blog/storage/framework ]; then

mkdir  /home/fleet/www/fleetmarshal/blog/storage/framework

fi

if ! [ -d /home/fleet/www/fleetmarshal/blog/storage/framework/session ]; then

mkdir  /home/fleet/www/fleetmarshal/blog/storage/framework/session

fi

if ! [ -d /home/fleet/www/fleetmarshal/blog/storage/framework/views ]; then

mkdir  /home/fleet/www/fleetmarshal/blog/storage/framework/views

fi

if ! [ -d /home/fleet/www/fleetmarshal/blog/storage/framework/cache ]; then

mkdir  /home/fleet/www/fleetmarshal/blog/storage/framework/cache

fi

# Run script, where copy project in temp folder 

/home/fleet/www/fleetmarshal/chdir.sh


chdir.sh :  copy project in temp folder


cp  /home/fleet/www/fleetmarshal/blog/.env.example  /home/fleet/www/fleetmarshal/blog/.env

cp /home/fleet/www/fleetmarshal/blog/docker/containers/app/.env.example   /home/fleet/www/fleetmarshal/blog/docker/containers/app/.env

cp /home/fleet/www/fleetmarshal/blog/docker/containers/db/.env.example   /home/fleet/www/fleetmarshal/blog/docker/containers/db/.env

# copy env var
cp /home/fleet/www/fleetmarshal/blog/.env /home/fleet/www/tmpfleet/blog/

cp /home/fleet/www/fleetmarshal/blog/docker/containers/app/.env /home/fleet/www/tmpfleet/blog/docker/containers/app/

cp /home/fleet/www/fleetmarshal/blog/docker/containers/db/.env  /home/fleet/www/tmpfleet/blog/docker/containers/db/

# copy migrations

cp -r /home/fleet/www/fleetmarshal/blog/database/migrations/  /home/fleet/www/tmpfleet/blog/database/migrations/

# copy other

rsync -rtuv /home/fleet/www/fleetmarshal/blog/ /home/fleet/www/tmpfleet/blog/


/home/fleet/www/tmpfleet/up.sh


 
up.sh:  run and configure containers


docker-compose -f /home/fleet/www/tmpfleet/blog/docker-compose-dev.yml build 

docker-compose -f  /home/fleet/www/tmpfleet/blog/docker-compose-dev.yml up -d

cd /home/fleet/www/tmpfleet/blog/

# I do this become were problem with permissions, so first script no make sense 

rm -rf storage/framework/cache

rm -rf storage/framework/views

rm -rf storage/framework/sessions

mkdir storage/framework/cache

mkdir storage/framework/views

mkdir storage/framework/sessions

docker-compose exec  app apt-get update

docker-compose  exec -u user  app  php artisan key:generate

docker-compose  exec -u user  app  php artisan config:cache

docker-compose  exec -u user  app  php artisan migrate 

docker-compose  exec -u user  app  php artisan db:seed


# What's more, i have questions : how configure bd

http://165.22.95.72:7004/

