# Jenkins with jira

## Step 1: Installing the Jira add-on

### 1)From the top navigation bar in Jira, choose Gear Icon > Manage apps.

![Screenshot_from_2020-02-26_12-06-21](/uploads/20bccec8a32727729bd46606d36c20a6/Screenshot_from_2020-02-26_12-06-21.png)

###  2) Entering "jenkins" or "jenkins integration" in the Search the Marketplace box. The "Jenkins Integration for Jira" will be listed in the search results

![Screenshot_from_2020-02-26_12-11-35](/uploads/2356b15c367866913223026aef330119/Screenshot_from_2020-02-26_12-11-35.png)

## Step 2: Installing the Jenkins add-on

###  1) The add-on for Jenkins "Jira Integration for Jenkins" is available for download form the release notes : https://docs.marvelution.org/jji/cloud/release-notes/jenkins

###  2) Navigate to the Manage Jenkins > Manage Plugins page in the web UI.

###  3) Click on the Advanced tab.
###  4) Choose the jenkins-jira-plugin-[version].hpi file under the Upload Plugin section.

![Screenshot_from_2020-02-26_12-16-24](/uploads/f3d16a174f0827a1c73ba16428697a42/Screenshot_from_2020-02-26_12-16-24.png)

###  5) Upload the plugin file.

###  6) If prompted, restart your application to have your change take effect.

## Step 3: Add your  Jenkins Site

After you installed the add-ons, you are ready to configure your first Jenkins Site and enable the jobs to synchronize.

###  1)From the top navigation in Jira, choose  Manage  Apps.

![Screenshot_from_2020-02-26_12-21-37](/uploads/da9aa62b21bf6a1085f57a7d9d84d0e1/Screenshot_from_2020-02-26_12-21-37.png)

###  2) Choose Jenkins Integration > Configuration and click on the Add Site button.
###  3) This brings up the Add Site dialog.

![Screenshot_from_2020-02-26_12-55-23](/uploads/e47e80786035b821de31369ee00a35f7/Screenshot_from_2020-02-26_12-55-23.png)

### Private Site

1) Provide a Name.

2) Optionally, check the Auto enable new Jobs checkbox if you want to enable all Jobs on the site by default, including future new jobs.

3) It is advised that you enable this, since jobs will only become available in Jira after Jenkins notifies Jira of it, and this can time some time depending the build workload on Jenkins.

4) Select Private accessibility option if your Jenkins site is behind a firewall or otherwise not accessible for job and build synchronization.

5) Provide the Display URL for UI features.
6) Save the configuration and open the Configuration options of the Jenkins site by clicking on the Configuration action of the site.
Follow the instructions in the dialog box to complete the registration of the site.


### Public Site

1) Selected the Type of the site.

2) Provide a Name.

3) Optionally, check the Auto enable new Jobs checkbox if you want to enable all Jobs on the site by default, including future new jobs.
3) Select Public accessibility.

Provide the Sync URL that should be used for synchronization and other background actions.

### Step 4: Enjoin the Insight

### 1) Open project 

![Screenshot_from_2020-02-26_13-12-35](/uploads/fce08fe167012003ed40f5e5d62ed0a2/Screenshot_from_2020-02-26_13-12-35.png)

### 2) Choise issue

![Screenshot_from_2020-02-26_13-26-08](/uploads/7b3cc9b95eb6d210a89e9f8dee9aba6a/Screenshot_from_2020-02-26_13-26-08.png)


### 3) there you can choise job in jenkins

![Screenshot_from_2020-02-26_13-36-05](/uploads/d2ccc97937624ededc30b5f59f382373/Screenshot_from_2020-02-26_13-36-05.png)


![Screenshot_from_2020-02-26_13-36-34](/uploads/c45466b865457c481625e614772cc6bb/Screenshot_from_2020-02-26_13-36-34.png)