## webhoock bitbucket with specific branch

### 1) Open settings bitbucket project

![Screenshot_from_2020-02-26_13-46-33](/uploads/81a8a182e4762382767b7e342b2eaa3d/Screenshot_from_2020-02-26_13-46-33.png)

### 2) Where you need find "webhook"

![Screenshot_from_2020-02-26_13-49-30](/uploads/4c55e938e5950e420c556fcd6a80a853/Screenshot_from_2020-02-26_13-49-30.png)

### 3) AND OPEN "Add webhook" 

![Screenshot_from_2020-02-26_14-04-48](/uploads/659e0bdfd64d18b7232efb3a1b3153aa/Screenshot_from_2020-02-26_14-04-48.png)

URL should have this format: http://ip-jenkins site/bitbucket-hook/

### 4) Install “Bitbucket Plugin” at your Jenkins

### 5)Configure your Jenkins : 


![Screenshot_from_2020-02-26_14-12-27](/uploads/8a1e9d673f7d8f53901b87da60286f71/Screenshot_from_2020-02-26_14-12-27.png)


![Screenshot_from_2020-02-26_14-12-01](/uploads/b7f74c5b674a6b94ce75dd9ea3e807f6/Screenshot_from_2020-02-26_14-12-01.png)